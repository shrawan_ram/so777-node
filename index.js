const express = require("express");
const bodyParser = require("body-parser");
var multer = require('multer');

var upload = multer();

// for parsing multipart/form-data
// var fs = require('fs');

// var type = upload.single('recfile');

const app = express();

const mysql = require('mysql');

// connection configurations
const mc = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'so777'
});

// connect to database
mc.connect();
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({
  limit: '50mb',
  extended: true,
  parameterLimit: 50000
}));

// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// app.use(function (req, res, next) {
//   res.set({
//     'Access-Control-Allow-Origin': '*',
//     'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
//     'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE'
//   })
//   next();
// });

app.use(upload.array());
app.use(express.static('public'));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

var approutes = require('./app/routes/approutes'); //importing route
approutes(app); //register the route

// simple route
// app.get("/", (req, res) => {
//   res.json({ message: "Welcome to bezkoder application." });
// });
// app.get("/home", (req, res) => {
//   res.json({ message: "Hello." });
// });

// set port, listen for requests
app.listen(3000, () => {
  console.log("Server is running on port 3000.");
});
