const { db } = require("../configs/db.config");
const global = require("../configs/constants.config");

exports.create_table = async (table, attributes, drop = false) => {
    table = global.DB.prefix + table;
    let sql = `CREATE TABLE IF NOT EXISTS ${table} (` + attributes.join(', ') + ')';
    return await db.query(sql);
}

exports.drop_table = async (table) => {
    table = global.DB.prefix + table;
    let success = await db.query(`DROP TABLE IF EXISTS ${table}`);
    console.log(table, 'dropped table: ', success);
}
