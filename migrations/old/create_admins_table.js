const { create_table } = require("../db.class");
const global = require("../../configs/constants.config");

exports.create_admins_table = async (drop = false) => {
    let attributes = [
        "id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY",
        "login VARCHAR(191) NOT NULL",
        "password VARCHAR(191) DEFAULT NULL",
        "email VARCHAR(191) DEFAULT NULL",
        "mobile VARCHAR(20) DEFAULT NULL",
        "role_id BIGINT UNSIGNED",
        "site_id BIGINT UNSIGNED",
        "verified_at DATETIME DEFAULT NULL",
        "created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP",
        "updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP",
        "FOREIGN KEY (site_id) REFERENCES " + global.DB.prefix + "sites(id) ON DELETE CASCADE",
        "FOREIGN KEY (role_id) REFERENCES " + global.DB.prefix + "roles(id) ON DELETE CASCADE",
        "CONSTRAINT admins_login_site_id_unique UNIQUE (login, site_id)"
    ];
    return await create_table("admins", attributes, drop);
}
