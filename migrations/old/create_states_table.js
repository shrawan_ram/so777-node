const { create_table } = require("../db.class");
const global = require("../../configs/constants.config");

exports.create_states_table = async (drop = false) => {
    let attributes = [
        "id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY",
        "name VARCHAR(191)",
        "short_name VARCHAR(5) DEFAULT NULL",
        "code VARCHAR(5) DEFAULT NULL",
        "country_id BIGINT UNSIGNED",
        "created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP",
        "updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP",
        "deleted_at DATETIME DEFAULT NULL",
        "FOREIGN KEY (country_id) REFERENCES " + global.DB.prefix + "countries(id) ON DELETE CASCADE"
    ];
    return await create_table("states", attributes, drop);
}
