const { create_table } = require("../db.class");
const global = require("../../configs/constants.config");

exports.create_roles_table = async (drop = false) => {
    let attributes = [
        "id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY",
        "name VARCHAR(191) NOT NULL",
        "site_id BIGINT UNSIGNED",
        "created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP",
        "updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP",
        "FOREIGN KEY (site_id) REFERENCES " + global.DB.prefix + "sites(id) ON DELETE CASCADE",
        "CONSTRAINT roles_name_site_id_unique UNIQUE (name, site_id)"
    ];
    return await create_table("roles", attributes, drop);
}
