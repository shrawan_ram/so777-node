const { create_table } = require("../db.class");
const global = require("../../configs/constants.config");

exports.create_site_categories_table = async (drop = false) => {
    let attributes = [
        "category_id BIGINT UNSIGNED DEFAULT NULL",
        "site_id BIGINT UNSIGNED DEFAULT NULL",
        "FOREIGN KEY (category_id) REFERENCES " + global.DB.prefix + "categories(id) ON DELETE CASCADE",
        "FOREIGN KEY (site_id) REFERENCES " + global.DB.prefix + "sites(id) ON DELETE CASCADE",
        "CONSTRAINT site_categories_category_id_site_id_unique UNIQUE (category_id, site_id)"
    ];
    return await create_table("site_categories", attributes, drop);
}
