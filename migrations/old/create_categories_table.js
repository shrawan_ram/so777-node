const { create_table } = require("../db.class");
const global = require("../../configs/constants.config");

exports.create_categories_table = async (drop = false) => {
    let attributes = [
        "id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY",
        "name VARCHAR(191) NOT NULL",
        "slug VARCHAR(191) DEFAULT NULL UNIQUE",
        "image VARCHAR(191) DEFAULT NULL",
        "icon VARCHAR(191) DEFAULT NULL",
        "parent BIGINT UNSIGNED DEFAULT NULL",
        "created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP",
        "updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP",
        "FOREIGN KEY (parent) REFERENCES " + global.DB.prefix + "categories(id) ON DELETE CASCADE"
    ];
    return await create_table("categories", attributes, drop);
}
