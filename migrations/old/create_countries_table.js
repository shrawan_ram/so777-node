const { create_table } = require("../db.class");

exports.create_countries_table = async (drop = false) => {
    let attributes = [
        "id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY",
        "name VARCHAR(191)",
        "short_name VARCHAR(5) DEFAULT NULL",
        "code VARCHAR(5) DEFAULT NULL",
        "created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP",
        "updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"
    ];
    return await create_table("countries", attributes, drop);
}
