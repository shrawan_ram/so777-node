const { create_table } = require("../db.class");
const global = require("../../configs/constants.config");

exports.create_cities_table = async (drop = false) => {
    let attributes = [
        "id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY",
        "name VARCHAR(191)",
        "short_name VARCHAR(5) DEFAULT NULL",
        "state_id BIGINT UNSIGNED",
        "created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP",
        "updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP",
        "deleted_at DATETIME DEFAULT NULL",
        "FOREIGN KEY (state_id) REFERENCES " + global.DB.prefix + "states(id) ON DELETE CASCADE"
    ];
    return await create_table("cities", attributes, drop);
}
