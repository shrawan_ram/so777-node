const { create_table } = require("../db.class");
const global = require("../../configs/constants.config");

exports.create_sites_table = async (drop = false) => {
    let attributes = [
        "id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY",
        "domain VARCHAR(191) NOT NULL UNIQUE",
        "site_title VARCHAR(191)",
        "site_tagline VARCHAR(191) DEFAULT NULL",
        "logo VARCHAR(191) DEFAULT NULL",
        "favicon VARCHAR(191) DEFAULT NULL",
        "address1 VARCHAR(191) DEFAULT NULL",
        "address2 VARCHAR(191) DEFAULT NULL",
        "city_id BIGINT UNSIGNED",
        "created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP",
        "updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP",
        "deleted_at DATETIME DEFAULT NULL",
        "FOREIGN KEY (city_id) REFERENCES " + global.DB.prefix + "cities(id) ON DELETE CASCADE"
    ];
    return await create_table("sites", attributes, drop);
}
