const { drop_table } = require("../db.class");
const { create_admins_table } = require("./create_admins_table");
const { create_categories_table } = require("./create_categories_table");
const { create_cities_table } = require("./create_cities_table");
const { create_countries_table } = require("./create_countries_table");
const { create_products_table } = require("./create_products_table");
const { create_roles_table } = require("./create_roles_table");
const { create_site_categories_table } = require("./create_site_categories");
const { create_sites_table } = require("./create_site_table");
const { create_states_table } = require("./create_states_table");

const { create_sliders_table } = require("./create_sliders_table");
const { create_upcoming_fixtures_table } = require("./create_upcoming_fixtures_table");


exports.run_migration = (drop = false) => {
    if (drop) {
        drop_table("products");
        drop_table("site_categories");
        drop_table("categories");
        drop_table("admins");
        drop_table("roles");
        drop_table("sites");
        drop_table("cities");
        drop_table("states");
        drop_table("countries");
    }

    create_countries_table();
    create_states_table();
    create_cities_table();
    create_sites_table();
    create_roles_table();
    create_admins_table();
    create_categories_table();
    create_site_categories_table();
    create_products_table();

    create_sliders_table();
    create_upcoming_fixtures_table();
}
