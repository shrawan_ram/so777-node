const { create_table } = require("../db.class");
const global = require("../../configs/constants.config");

exports.create_sliders_table = async (drop = false) => {
    let attributes = [
        "id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY",
        "name VARCHAR(191) NOT NULL",
        "image VARCHAR(255) DEFAULT NULL",
        "created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP",
        "updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP",
    ];
    return await create_table("sliders", attributes, drop);
}
