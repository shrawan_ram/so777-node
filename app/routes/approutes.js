'use strict';
// import passport and passport-jwt modules
const passport = require('passport');
const Auth = require('../controller/AuthController');
// const Setting = require('../controller/SettingController');
const Slider = require('../controller/SliderController');
const Category = require('../controller/CategoryController');
const Gametype = require('../controller/GameTypeController');
const Game = require('../controller/GameController');
const Match = require('../controller/MatchController');
const SettleBat = require('../controller/SettleBatController');
const Role = require('../controller/RoleController');
const Admin = require('../controller/AdminController');
const Page = require('../controller/PageController');
const User = require('../controller/UserController');
const Bat = require('../controller/BatController');
const Wallet = require('../controller/WalletController');
const WalletHistory = require('../controller/WalletHistoryController');
var multer = require('multer');
var upload = multer({ dest: 'upload/' });
module.exports = function (app) {
    /**
     * Auth Routes
     */
    app.route('/api/admin/login').post(Auth.admin_login);
    app.route('/api/admin/profile').get(passport.authenticate('jwt', { session: false }), function (req, res) {
        // console.log(next);
        res.json({ msg: 'Congrats! You are seeing this because you are authorized', user: req.user });
    })
    app.route('/api/user/login').post(Auth.user_login);

    // Slider Routes
    app.route('/api/slider')
        .get(Slider.get_all_slider);

    app.route('/api/add-slider')
        .post(Slider.create);

    app.route('/api/slider/:id')
        .get(Slider.details)
        .put(Slider.update);

    app.route('/api/remove-slider')
        .delete(Slider.remove);


    // Role Routes
    app.route('/api/role')
        .get(Role.get_all_role);

    app.route('/api/add-role')
        .post(Role.create);

    app.route('/api/update-role/:id')
        .put(Role.update);

    app.route('/api/remove-role')
        .delete(Role.remove);


    // Admin Routes 
    app.route('/api/admin')
        .get(Admin.get_all_admin);

    app.route('/api/add-admin')
        .post(Admin.create);

    app.route('/api/admin/:id')
        .get(Admin.details)
        .put(Admin.update);

    app.route('/api/remove-admin')
        .delete(Admin.remove);

    // Page Routes
    app.route('/api/page')
        .get(Page.list_all_page);

    app.route('/api/add-page')
        .post(Page.create);

    app.route('/api/page/:id')
        .get(Page.detailsById)
        .put(Page.update);

    app.route('/api/remove-page')
        .delete(Page.remove);

    app.route('/api/page/info/:slug')
        .get(Page.details);

    // User Routes 
    app.route('/api/user')
        .get(User.get_all_user);

    app.route('/api/add-user')
        .post(User.create);

    app.route('/api/user/:id')
        .put(User.update)
        .put(User.updatePassword)
        .get(User.details);

    app.route('/api/user-password/:id')
        .put(User.updatePassword)

    app.route('/api/remove-user')
        .delete(User.remove);

    // Category Routes
    app.route('/api/category')
        .get(Category.list_all_category);

    app.route('/api/all-category')
        .get(Category.list_category);

    app.route('/api/add-category')
        .post(upload.single('image'), Category.create);

    app.route('/api/category/:id')
        .get(Category.detailsById)
        .put(Category.update);

    app.route('/api/remove-category')
        .delete(Category.remove);

    app.route('/api/category/info/:slug')
        .get(Category.details);

    // Game Type Routes
    app.route('/api/gametype')
        .get(Gametype.list_all_gametype);

    app.route('/api/all-gametype')
        .get(Gametype.list_gametype);

    app.route('/api/add-gametype')
        .post(Gametype.create);

    app.route('/api/gametype/:id')
        .get(Gametype.detailsById)
        .put(Gametype.update);

    app.route('/api/remove-gametype')
        .delete(Gametype.remove);

    app.route('/api/gametype/info/:slug')
        .get(Gametype.details);

    // Game Routes
    app.route('/api/game')
        .get(Game.list_all_game);

    app.route('/api/add-game')
        .post(Game.create);

    app.route('/api/game/:id')
        .get(Game.detailsById)
        .put(Game.update);

    app.route('/api/remove-game')
        .delete(Game.remove);

    app.route('/api/game/info/:slug')
        .get(Game.details);


    // Match Routes
    app.route('/api/match')
        .get(Match.list_all_match);

    app.route('/api/add-match')
        .post(Match.create);

    app.route('/api/match/:id')
        .get(Match.detailsById)
        .put(Match.update);

    app.route('/api/remove-match')
        .delete(Match.remove);

    app.route('/api/match/info/:id')
        .get(Match.details);


    // Settle Bat Routes
    app.route('/api/settled')
        .get(SettleBat.list_all_match);

    app.route('/api/add-settled')
        .post(SettleBat.create);

    app.route('/api/settled/:id')
        .get(SettleBat.detailsById)
        .put(SettleBat.update);

    app.route('/api/remove-settled')
        .delete(SettleBat.remove);

    app.route('/api/settled/info/:id')
        .get(SettleBat.details);


    // Bat Routes
    app.route('/api/bat')
        .get(Bat.list_all_bat);

    app.route('/api/add-bat')
        .post(Bat.create);

    app.route('/api/bat/:id')
        .get(Bat.detailsById)
        .put(Bat.update);

    app.route('/api/userbats/:id')
        .get(Bat.batsByuserId)


    app.route('/api/remove-bat')
        .delete(Bat.remove);

    app.route('/api/bat/info/:id')
        .get(Bat.details);

    // Wallet Routes
    app.route('/api/wallet')
        .get(Wallet.list_all_wallet);

    app.route('/api/add-wallet')
        .post(Wallet.create);

    app.route('/api/wallet/:id')
        .get(Wallet.detailsById)
        .put(Wallet.update);

    app.route('/api/remove-wallet')
        .delete(Wallet.remove);

    app.route('/api/wallet/info/:id')
        .get(Wallet.details);


    // Wallet History Routes
    app.route('/api/wallet-history')
        .get(WalletHistory.list_all_wallet);

    app.route('/api/add-wallet-history')
        .post(WalletHistory.create);

    app.route('/api/wallet-history/:id')
        .get(WalletHistory.detailsById)
        .put(WalletHistory.update);

    app.route('/api/remove-wallet-history')
        .delete(WalletHistory.remove);

    app.route('/api/wallet-history/info/:id')
        .get(WalletHistory.details);



};