'use strict';

var Game = require('../model/GameModel.js');

exports.list_all_game = function (req, res) {
    Game.getAllGame(req, function (err, app) {
        if (err)
            res.send(err);

        res.send(app);
    });
};

exports.create = function (req, res) {
    var new_game = req.body;
    console.log('gdfgdfgdg esdf sdf f', new_game);
    new_game.slug = new_game.slug ? new_game.slug.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '') : new_game.name.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');

    Game.createGame(new_game, function (err, app) {
        if (err)
            res.send(err);

        console.log(app);
        res.send(app);
    });
};

exports.remove = (req, res) => {
    let n_ids = req.body.ids;

    if (n_ids.length) {
        Game.remove(n_ids, (err, app) => {
            if (err) res.send(err);
            res.json(app);
        });
    }
};

exports.details = function (req, res) {
    console.log('success');
    Game.getGameById(req.params.slug, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};
exports.detailsById = function (req, res) {
    console.log('success');
    Game.getGameDetail(req.params.id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

exports.update = function (req, res) {
    var new_game = req.body;
    // new_game.slug = new_game.slug ? new_game.slug.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '') : new_game.name.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');

    Game.updateById(req.params.id, new_game, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

