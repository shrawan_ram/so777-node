'use strict';

var Page = require('../model/PageModel.js');

exports.list_all_page = function (req, res) {
    Page.getAllPage(req, function (err, app) {
        if (err)
            res.send(err);

        res.send(app);
    });
};

exports.create = function (req, res) {
    var new_page = req.body;
    new_page.slug = new_page.slug ? new_page.slug.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '') : new_page.name.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');

    Page.createPage(new_page, function (err, app) {
        if (err)
            res.send(err);

        console.log(app);
        res.send(app);
    });
};

exports.remove = (req, res) => {
    let n_ids = req.body.ids;

    if (n_ids.length) {
        Page.remove(n_ids, (err, app) => {
            if (err) res.send(err);
            res.json(app);
        });
    }
};

exports.details = function (req, res) {
    console.log('success');
    Page.getPageById(req.params.slug, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};
exports.detailsById = function (req, res) {
    console.log('success');
    Page.getPageDetail(req.params.id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

exports.update = function (req, res) {
    var new_page = req.body;
    new_page.slug = new_page.slug ? new_page.slug.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '') : new_page.name.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');

    Page.updateById(req.params.id, new_page, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

