'use strict';

var Category = require('../model/CategoryModel.js');

exports.list_category = function (req, res) {
    Category.getCategory(req, function (err, app) {
        if (err)
            res.send(err);

        res.send(app);
    });
};

exports.list_all_category = function (req, res) {
    Category.getAllCategory(req, function (err, app) {
        if (err)
            res.send(err);

        res.send(app);
    });
};

exports.create = function (req, res) {
    // console.log('request', req);
    var new_category = req.body;
    new_category.slug = new_category.slug ? new_category.slug.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '') : new_category.name.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');

    Category.createCategory(new_category, function (err, app) {
        if (err)
            res.send(err);

        console.log(app);
        res.send(app);
    });
};

exports.remove = (req, res) => {
    let n_ids = req.body.ids;

    if (n_ids.length) {
        Category.remove(n_ids, (err, app) => {
            if (err) res.send(err);
            res.json(app);
        });
    }
};

exports.details = function (req, res) {
    console.log('success');
    Category.getCategoryById(req.params.slug, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};
exports.detailsById = function (req, res) {
    console.log('success');
    Category.getCategoryDetail(req.params.id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

exports.update = function (req, res) {
    var new_category = req.body;
    // new_category.slug = new_category.slug ? new_category.slug.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '') : new_category.name.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');

    Category.updateById(req.params.id, new_category, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

