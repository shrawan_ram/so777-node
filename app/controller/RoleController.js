'use strict';

var Role = require('../model/RoleModel.js');

exports.get_all_role = function (req, res) {
    Role.getAllRole(req, function (err, app) {
        if (err)
            res.send(err);

        res.send(app);
    });
};

exports.create = function (req, res) {
    var new_role = req.body;

    Role.createRole(new_role, function (err, app) {
        if (err)
            res.send(err);

        console.log(app);
        res.send(app);
    });
};

exports.remove = (req, res) => {
    let n_ids = req.body.ids;

    if (n_ids.length) {
        Role.remove(n_ids, (err, app) => {
            if (err) res.send(err);
            res.json(app);
        });
    }
};

exports.update = function (req, res) {
    Role.updateById(req.params.id, req.body, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

