'use strict';

var Admin = require('../model/AdminModel.js');

exports.get_all_admin = function (req, res) {
    Admin.getAllAdmin(req, function (err, app) {
        if (err)
            res.send(err);

        res.send(app);
    });
};

exports.create = function (req, res) {
    var new_admin = req.body;
    Admin.createAdmin(new_admin, function (err, app) {
        if (err)
            res.send(err);

        console.log(app);
        res.send(app);
    });
};

exports.remove = (req, res) => {
    let n_ids = req.body.ids;

    if (n_ids.length) {
        Admin.remove(n_ids, (err, app) => {
            if (err) res.send(err);
            res.json(app);
        });
    }
};

exports.details = function (req, res) {
    Admin.getDetail(req.params.id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

exports.update = function (req, res) {
    Admin.updateById(req.params.id, req.body, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

