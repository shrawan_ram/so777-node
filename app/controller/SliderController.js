'use strict';

var Slider = require('../model/SliderModel.js');

exports.get_all_slider = function (req, res) {
    Slider.getAllSlider(req, function (err, app) {
        if (err)
            res.send(err);

        res.send(app);
    });
};

exports.create = function (req, res) {
    var new_slider = req.body;

    Slider.createSlider(new_slider, function (err, app) {
        if (err)
            res.send(err);

        console.log(app);
        res.send(app);
    });
};

exports.remove = (req, res) => {
    let n_ids = req.body.ids;

    if (n_ids.length) {
        Slider.remove(n_ids, (err, app) => {
            if (err) res.send(err);
            res.json(app);
        });
    }
};

exports.details = function (req, res) {
    Slider.getDetail(req.params.id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

exports.update = function (req, res) {
    Slider.updateById(req.params.id, req.body, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

