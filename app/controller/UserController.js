'use strict';

var User = require('../model/UserModel.js');

exports.get_all_user = function (req, res) {
    User.getAllUser(req, function (err, app) {
        if (err)
            res.send(err);

        res.send(app);
    });
};

exports.create = function (req, res) {
    var new_user = req.body;

    User.createUser(new_user, function (err, app) {
        if (err)
            res.send(err);

        console.log(app);
        res.send(app);
    });
};

exports.remove = (req, res) => {
    let n_ids = req.body.ids;

    if (n_ids.length) {
        User.remove(n_ids, (err, app) => {
            if (err) res.send(err);
            res.json(app);
        });
    }
};


exports.details = function (req, res) {
    User.getDetail(req.params.id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

exports.update = function (req, res) {
    User.updateById(req.params.id, req.body, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

exports.updatePassword = function (req, res) {
    User.updatePasswordById(req.params.id, req.body, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

