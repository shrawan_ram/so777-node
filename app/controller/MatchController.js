'use strict';

var Match = require('../model/MatchModel.js');

exports.list_all_match = function (req, res) {
    Match.getAllMatch(req, function (err, app) {
        if (err)
            res.send(err);

        res.send(app);
    });
};

exports.create = function (req, res) {
    var new_match = req.body;
    // new_match.slug = new_match.slug ? new_match.slug.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '') : new_match.name.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');

    Match.createMatch(new_match, function (err, app) {
        if (err)
            res.send(err);

        console.log(app);
        res.send(app);
    });
};

exports.remove = (req, res) => {
    let n_ids = req.body.ids;

    if (n_ids.length) {
        Match.remove(n_ids, (err, app) => {
            if (err) res.send(err);
            res.json(app);
        });
    }
};

exports.details = function (req, res) {
    console.log('success');
    Match.getMatchById(req.params.event_id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};
exports.detailsById = function (req, res) {
    console.log('success');
    Match.getMatchDetail(req.params.id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

exports.update = function (req, res) {
    var new_match = req.body;
    // new_match.slug = new_match.slug ? new_match.slug.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '') : new_match.name.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');

    Match.updateById(req.params.id, new_match, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

