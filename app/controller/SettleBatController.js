'use strict';

var SettleBat = require('../model/SettleBatModel.js');

exports.list_all_match = function (req, res) {
    SettleBat.getAllMatch(req, function (err, app) {
        if (err)
            res.send(err);

        res.send(app);
    });
};

exports.create = function (req, res) {
    var new_match = req.body;
    // new_match.slug = new_match.slug ? new_match.slug.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '') : new_match.name.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');

    SettleBat.createMatch(new_match, function (err, app) {
        if (err)
            res.send(err);

        console.log(app);
        res.send(app);
    });
};

exports.remove = (req, res) => {
    let n_ids = req.body.ids;

    if (n_ids.length) {
        SettleBat.remove(n_ids, (err, app) => {
            if (err) res.send(err);
            res.json(app);
        });
    }
};

exports.details = function (req, res) {
    console.log('success');
    SettleBat.getMatchById(req.params.event_id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};
exports.detailsById = function (req, res) {
    console.log('success');
    SettleBat.getMatchDetail(req.params.id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

exports.update = function (req, res) {
    var new_match = req.body;
    // new_match.slug = new_match.slug ? new_match.slug.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '') : new_match.name.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');

    SettleBat.updateById(req.params.id, new_match, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

