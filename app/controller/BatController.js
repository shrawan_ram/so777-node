'use strict';

var Bat = require('../model/BatModel.js');

exports.list_all_bat = function (req, res) {
    Bat.getAllBat(req, function (err, app) {
        if (err)
            res.send(err);

        res.send(app);
    });
};

exports.create = function (req, res) {
    var new_bat = req.body;

    Bat.createBat(new_bat, function (err, app) {
        if (err)
            res.send(err);

        console.log(app);
        res.send(app);
    });
};

exports.remove = (req, res) => {
    let n_ids = req.body.ids;

    if (n_ids.length) {
        Page.remove(n_ids, (err, app) => {
            if (err) res.send(err);
            res.json(app);
        });
    }
};

exports.details = function (req, res) {
    Bat.getBatById(req.params.id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

exports.detailsById = function (req, res) {
    Bat.getBatDetail(req.params.id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

exports.batsByuserId = function (req, res) {
    Bat.getUserBats(req.query, req.params.id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};
exports.update = function (req, res) {
    var new_bat = req.body;

    Bat.updateById(req.params.id, new_bat, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

