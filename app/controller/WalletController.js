'use strict';

var Wallet = require('../model/WalletModel.js');

exports.list_all_wallet = function (req, res) {
    Wallet.getAllWallet(req, function (err, app) {
        if (err)
            res.send(err);

        res.send(app);
    });
};

exports.create = function (req, res) {
    var new_wallet = req.body;

    Wallet.createWallet(new_wallet, function (err, app) {
        if (err)
            res.send(err);

        console.log(app);
        res.send(app);
    });
};

exports.remove = (req, res) => {
    let n_ids = req.body.ids;

    if (n_ids.length) {
        Page.remove(n_ids, (err, app) => {
            if (err) res.send(err);
            res.json(app);
        });
    }
};

exports.details = function (req, res) {
    Wallet.getWalletById(req.params.id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};
exports.detailsById = function (req, res) {
    Wallet.getWalletDetail(req.params.id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

exports.update = function (req, res) {
    var new_wallet = req.body;

    Wallet.updateById(req.params.id, new_wallet, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

