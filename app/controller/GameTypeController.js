'use strict';

var Gametype = require('../model/GameTypeModel.js');

exports.list_gametype = function (req, res) {
    Gametype.getGametype(req, function (err, app) {
        if (err)
            res.send(err);

        res.send(app);
    });
};
exports.list_all_gametype = function (req, res) {
    Gametype.getAllGametype(req, function (err, app) {
        if (err)
            res.send(err);
        res.send(app);
    });
};

exports.create = function (req, res) {
    var new_gametype = req.body;
    new_gametype.slug = new_gametype.slug ? new_gametype.slug.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '') : new_gametype.name.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');

    Gametype.createGametype(new_gametype, function (err, app) {
        if (err)
            res.send(err);

        console.log(app);
        res.send(app);
    });
};

exports.remove = (req, res) => {
    let n_ids = req.body.ids;

    if (n_ids.length) {
        Gametype.remove(n_ids, (err, app) => {
            if (err) res.send(err);
            res.json(app);
        });
    }
};

exports.details = function (req, res) {
    Gametype.getGametypeById(req.params.slug, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};
exports.detailsById = function (req, res) {
    Gametype.getGametypeDetail(req.params.id, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

exports.update = function (req, res) {
    var new_gametype = req.body;
    // new_gametype.slug = new_gametype.slug ? new_gametype.slug.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '') : new_gametype.name.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');

    Gametype.updateById(req.params.id, new_gametype, function (err, app) {
        if (err)
            res.send(err);
        res.json(app);
    });
};

