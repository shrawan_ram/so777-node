'use strict';

var Auth = require('../model/AuthModel');

exports.admin_login = function (req, res) {
    Auth.adminLogin(req.body, function (err, app) {
        if (err) {
            res.status(401).send(err);
        } else {
            res.send(app);
        }
    });
};

exports.user_login = function (req, res) {
    Auth.userLogin(req.body, function (err, app) {
        if (err) {
            res.status(401).send(err);
        } else {
            res.send(app);
        }
    });
};