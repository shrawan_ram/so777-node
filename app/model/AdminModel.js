'user strict';
var sql = require('./db.js');
const express = require("express");
const bcrypt = require('bcryptjs')

//Category object constructor
var Admin = function (app) {
    this.name = app.name;
    this.created_at = new Date();
    this.updated_at = new Date();
};
Admin.getDetail = function (id, result) {
    sql.query("SELECT * FROM `admins` WHERE `id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Admin.setPassword = async (password) => {
    const salt = await bcrypt.genSalt(10);
    // now we set user password to hashed password
    let pass = await bcrypt.hash(password, salt);
    console.log('password', pass);
    return pass;
}
Admin.createAdmin = async (newAdmin, result) => {
    console.log(newAdmin);
    let response;
    let password = await Admin.setPassword(newAdmin.password);
    let admin = {
        name: newAdmin.name,
        user_name: newAdmin.user_name,
        mobile: newAdmin.mobile,
        password: password,
        email: newAdmin.email,
        role_id: newAdmin.role_id,
    }
    console.log(admin);
    sql.query("SELECT COUNT(*) AS total FROM admins WHERE user_name = ?", [newAdmin.user_name], function (err, res) {

        if (res[0].total == 0) {
            sql.query("INSERT INTO admins SET ?", admin);

            let adminid;
            sql.query("SELECT * FROM `admins` WHERE `user_name` = ?", [newAdmin.user_name], function (err, res1) {
                adminid = res1[0].id;
                console.log('sdzxkjfdk', adminid);

                console.log('admin_id', adminid);
                let domain = {
                    name: newAdmin.domain,
                    admin_id: adminid,
                }
                sql.query("INSERT INTO domains SET ?", domain);
            });

            response = {
                status: true,
                message: 'Success! New record added.'
            }
            result(null, response)
        } else {
            response = {
                status: true,
                message: 'User name already exits.'
            }
            result(null, response)
        }
    });
};
Admin.getAllAdmin = function (req, result) {
    let numRows,
        queryPagination,
        numPerPage = req.query.limit ?? 25,
        page = parseInt(req.params.page) || 1,
        numPages,
        skip = (page - 1) * numPerPage,
        limit = skip + ',' + numPerPage,
        where = req.query.s ? `  name LIKE '%${req.query.s}%'` : '  1';

    let orderBy = req.query.order_by ? `ORDER BY ${req.query.order_by}` : 'ORDER BY `id` DESC'

    sql.query("SELECT COUNT(*) AS numRows FROM `admins` WHERE" + where, (err, res) => {
        if (err) {
            result(null, err);
        } else {
            numRows = res[0].numRows;
            numPages = Math.ceil(numRows / numPerPage);

            sql.query("SELECT * FROM admins WHERE `role_id` IN (4) AND" + where + " " + orderBy + " LIMIT " + limit, function (err, res2) {
                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                } else {
                    let responsePayload = {
                        results: res2
                    }
                    if (page <= numPages) {
                        responsePayload.pagination = {
                            current: page,
                            perPage: numPerPage,
                            numPages: numPages,
                            firstItem: skip + 1,
                            lastItem: skip + res2.length,
                            totalRecords: numRows,
                            previous: page > 1 ? page - 1 : undefined,
                            next: page < numPages ? page + 1 : undefined
                        }
                    }
                    result(null, responsePayload);
                }
            });
        }
    });
};
Admin.updateById = async (id, newAdmin, result) => {
    let password = await Admin.setPassword(newAdmin.password);
    admin = {
        name: newAdmin.name,
        user_name: newAdmin.user_name,
        mobile: newAdmin.mobile,
        password: password,
        email: newAdmin.email,
        role_id: newAdmin.role_id,
    }
    sql.query("UPDATE admins SET ? WHERE id = ?", [admin, id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, { status: true, message: 'Record updated successfully.' });
        }
    });
};
Admin.remove = function (ids, result) {
    console.log([ids]);
    sql.query("DELETE FROM admins WHERE id IN (?)", [ids], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log(res);
            result(null, res);
        }
    });
};

module.exports = Admin;
