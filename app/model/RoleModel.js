'user strict';
var sql = require('./db.js');

//Category object constructor
var Role = function (app) {
    this.name = app.name;
    this.created_at = new Date();
    this.updated_at = new Date();
};
Role.getDetail = function (id, result) {
    sql.query("SELECT * FROM `roles` WHERE `id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Role.createRole = function (newRole, result) {
    let response;
    sql.query("INSERT INTO roles SET ?", newRole);
    response = {
        status: true,
        message: 'Success! New record added.'
    }
    result(null, response)
};
Role.getAllRole = function (req, result) {
    let where = '';
    if (req.query.role_id) {
        where = `id > ${req.query.role_id}`;
    }
    sql.query(`SELECT * FROM roles WHERE id IN (3) OR ${where}`, function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};
Role.updateById = function (id, role, result) {
    sql.query("UPDATE roles SET ? WHERE id = ?", [role, id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, { status: true, message: 'Record updated successfully.' });
        }
    });
};
Role.remove = function (ids, result) {
    console.log([ids]);
    sql.query("DELETE FROM roles WHERE id IN (?)", [ids], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log(res);
            result(null, res);
        }
    });
};

module.exports = Role;
