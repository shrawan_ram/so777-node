'user strict';
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const sql = require('./db.js')

/**
 * Passport Authentication
 */
// import passport and passport-jwt modules
const passport = require('passport');
const passportJWT = require('passport-jwt');

// ExtractJwt to help extract the token
let ExtractJwt = passportJWT.ExtractJwt;
// JwtStrategy which is the strategy for the authentication
let JwtStrategy = passportJWT.Strategy;
let jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = 'wowwow';

// lets create our strategy for web token
let strategy = new JwtStrategy(jwtOptions, function (jwt_payload, next) {
    getUser({ id: jwt_payload.id }, (err, user) => {
        if (err) {
            next(err, null)
        } else if (user) {
            next(null, user);
        } else {
            next(null, false);
        }
    });
});
// use the strategy
passport.use(strategy);

//App object constructor
var Auth = function () {
    this.created_at = new Date();
};

const getAdmin = (obj, result) => {
    sql.query("SELECT * FROM `admins` WHERE role_id NOT IN (3) AND ?", [obj], (err, res) => {
        if (err) {
            result(err, null)
        } else if (!res.length) {
            result(null, null)
        } else {
            result(null, res[0])
        }
    })
};
const getUser = (obj, result) => {
    sql.query("SELECT * FROM `admins` WHERE role_id IN (3) AND ?", [obj], (err, res) => {
        console.log(res);
        if (err) {
            result(err, null)
        } else if (!res.length) {
            result(null, null)
        } else {
            result(null, res[0])
        }
    })
};

Auth.adminLogin = function (newApp, result) {
    const { user_name, password } = newApp;

    if (user_name && password) {
        getAdmin({ user_name: user_name }, (err, user) => {
            console.log(user);
            if (err) {
                result(err, null)
            } else if (!user) {
                result(null, { status: false, msg: 'Login failed! Username is not exists.' })
            } else {
                bcrypt.compare(password, user.password, function (err, isMatch) {
                    if (err) {
                        result(err, null)
                    } else if (!isMatch) {
                        result(null, { status: false, msg: 'Login failed! Password is not matched.' })
                    } else {
                        let payload = { id: user.id };
                        let token = jwt.sign(payload, jwtOptions.secretOrKey)
                        result(null, { status: true, msg: 'Success! You\'re logged in.', token: token, data: user })
                    }
                })
            }
        })
    } else {
        result({ error: 'Required field missing.' }, null)
    }
};

Auth.userLogin = function (newApp, result) {
    const { user_name, password } = newApp;

    if (user_name && password) {
        getUser({ user_name: user_name }, (err, user) => {
            if (err) {
                result(err, null)
            } else if (!user) {
                result(null, { status: false, msg: 'Login failed! Username is not exists.' })
            } else {
                bcrypt.compare(password, user.password, function (err, isMatch) {
                    if (err) {
                        result(err, null)
                    } else if (!isMatch) {
                        result(null, { status: false, msg: 'Login failed! Password is not matched.' })
                    } else {
                        let payload = { id: user.id };
                        let token = jwt.sign(payload, jwtOptions.secretOrKey)
                        result(null, { status: true, msg: 'Success! You\'re logged in.', token: token, data: user })
                    }
                })
            }
        })
    } else {
        result({ error: 'Required field missing.' }, null)
    }
};

module.exports = Auth;
