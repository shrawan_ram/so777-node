'user strict';
var sql = require('./db.js');

//Game object constructor
var Game = function (app) {
    this.name = app.name;
    this.slug = app.slug;
    this.created_at = new Date();
    this.updated_at = new Date();
};
Game.getGameById = function (slug, result) {
    sql.query("SELECT * FROM `games` WHERE `slug` = ?", [slug], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Game.getGameDetail = function (id, result) {
    sql.query("SELECT * FROM `games` WHERE `id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Game.getDetails = function (page, result) {
    sql.query("SELECT * FROM `games` WHERE `slug` = ?", [page], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Game.createGame = function (newGame, result) {
    sql.query("SELECT COUNT(*) AS total FROM games WHERE slug = ?", [newGame.slug], function (err, res) {
        if (err) {
            result(err, null);
        }
        else {
            let response;
            if (res[0].total == 0) {
                sql.query("INSERT INTO games SET ?", newGame);
                response = {
                    status: true,
                    message: 'Success! New record added.'
                }
            } else {
                response = {
                    status: false,
                    errors: [
                        'Error! Game already exists.'
                    ]
                }
            }
            result(null, response)
        }
    });

};
Game.getAllGame = function (req, result) {
    let numRows,
        queryPagination,
        numPerPage = req.query.limit ?? 25,
        page = parseInt(req.params.page) || 1,
        numPages,
        skip = (page - 1) * numPerPage,
        limit = skip + ',' + numPerPage,
        where = req.query.s ? ` games.name LIKE '%${req.query.s}%'` : ' 1',
        where1 = req.query.category_id ? ` games.category_id = ${req.query.category_id}` : ' 1',
        where2 = req.query.game_type_id ? ` games.game_type_id = ${req.query.game_type_id}` : ' 1';

    let orderBy = req.query.order_by ? `ORDER BY ${req.query.order_by}` : 'ORDER BY `id` DESC'

    sql.query("SELECT COUNT(*) AS numRows FROM `games` WHERE" + where, (err, res) => {
        if (err) {
            result(null, err);
        } else {
            numRows = res[0].numRows;
            numPages = Math.ceil(numRows / numPerPage);

            sql.query("SELECT games.*, categories.name as category_name,game_types.name as game_type_name FROM games LEFT JOIN categories ON games.category_id = categories.id LEFT JOIN game_types ON games.game_type_id = game_types.id WHERE" + where + " AND " + where1 + " AND " + where2 + " " + orderBy + " LIMIT " + limit, function (err, res2) {
                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                } else {
                    let responsePayload = {
                        results: res2
                    }
                    if (page <= numPages) {
                        responsePayload.pagination = {
                            current: page,
                            perPage: numPerPage,
                            numPages: numPages,
                            firstItem: skip + 1,
                            lastItem: skip + res2.length,
                            totalRecords: numRows,
                            previous: page > 1 ? page - 1 : undefined,
                            next: page < numPages ? page + 1 : undefined
                        }
                    }
                    result(null, responsePayload);
                }
            });
        }
    });
};
Game.updateById = function (id, page, result) {
    sql.query("UPDATE games SET ? WHERE id = ?", [page, id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, { status: true, message: 'Record updated successfully.' });
        }
    });
};
Game.remove = function (ids, result) {
    sql.query("DELETE FROM games WHERE id IN (?)", [ids], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

module.exports = Game;
