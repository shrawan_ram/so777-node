'user strict';
var sql = require('./db.js');

//Category object constructor
var Page = function (app) {
    this.name = app.name;
    this.slug = app.slug;
    this.created_at = new Date();
    this.updated_at = new Date();
};
Page.getPageById = function (slug, result) {
    sql.query("SELECT * FROM `pages` WHERE `slug` = ?", [slug], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Page.getPageDetail = function (id, result) {
    sql.query("SELECT * FROM `pages` WHERE `id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Page.getDetails = function (page, result) {
    sql.query("SELECT * FROM `pages` WHERE `slug` = ?", [page], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Page.createPage = function (newPage, result) {
    sql.query("SELECT COUNT(*) AS total FROM pages WHERE slug = ?", [newPage.slug], function (err, res) {
        if (err) {
            result(err, null);
        }
        else {
            let response;
            if (res[0].total == 0) {
                sql.query("INSERT INTO pages SET ?", newPage);
                response = {
                    status: true,
                    message: 'Success! New record added.'
                }
            } else {
                response = {
                    status: false,
                    errors: [
                        'Error! page already exists.'
                    ]
                }
            }
            result(null, response)
        }
    });

};
Page.getAllPage = function (req, result) {
    let numRows,
        queryPagination,
        numPerPage = req.query.limit ?? 25,
        page = parseInt(req.params.page) || 1,
        numPages,
        skip = (page - 1) * numPerPage,
        limit = skip + ',' + numPerPage,
        where = req.query.s ? ` WHERE name LIKE '%${req.query.s}%'` : ' WHERE 1';

    let orderBy = req.query.order_by ? `ORDER BY ${req.query.order_by}` : 'ORDER BY `id` DESC'

    sql.query("SELECT COUNT(*) AS numRows FROM `pages`" + where, (err, res) => {
        if (err) {
            result(null, err);
        } else {
            numRows = res[0].numRows;
            numPages = Math.ceil(numRows / numPerPage);

            sql.query("SELECT * FROM pages" + where + " " + orderBy + " LIMIT " + limit, function (err, res2) {
                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                } else {
                    let responsePayload = {
                        results: res2
                    }
                    if (page <= numPages) {
                        responsePayload.pagination = {
                            current: page,
                            perPage: numPerPage,
                            numPages: numPages,
                            firstItem: skip + 1,
                            lastItem: skip + res2.length,
                            totalRecords: numRows,
                            previous: page > 1 ? page - 1 : undefined,
                            next: page < numPages ? page + 1 : undefined
                        }
                    }
                    result(null, responsePayload);
                }
            });
        }
    });
};
Page.updateById = function (id, page, result) {
    sql.query("UPDATE pages SET ? WHERE id = ?", [page, id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, { status: true, message: 'Record updated successfully.' });
        }
    });
};
Page.remove = function (ids, result) {
    sql.query("DELETE FROM pages WHERE id IN (?)", [ids], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

module.exports = Page;
