'user strict';
var sql = require('./db.js');

//Category object constructor
var WalletHistory = function (app) {
    this.user = app.user;
    this.user_id = app.user_id;
    this.created_at = new Date();
    this.updated_at = new Date();
};
WalletHistory.getWalletById = function (slug, result) {
    sql.query("SELECT * FROM `wallet_histories` WHERE `user_id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res)
        }
    });
}
WalletHistory.getWalletDetail = function (id, result) {
    sql.query("SELECT * FROM `wallet_histories` WHERE `user_id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res)
        }
    });
}
WalletHistory.getDetails = function (page, result) {
    sql.query("SELECT * FROM `wallet_histories` WHERE `user_id` = ?", [page], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res)
        }
    });
}
WalletHistory.createWallet = function (newWallet, result) {
    sql.query("INSERT INTO wallet_histories SET ?", newWallet);
    response = {
        status: true,
        message: 'Success! New record added.'
    }
    result(null, response)

};
WalletHistory.getAllWallet = function (req, result) {
    let numRows,
        queryPagination,
        numPerPage = req.query.limit ?? 25,
        page = parseInt(req.params.page) || 1,
        numPages,
        skip = (page - 1) * numPerPage,
        limit = skip + ',' + numPerPage,
        where = req.query.s ? ` WHERE user LIKE '%${req.query.s}%'` : ' WHERE 1';

    let orderBy = req.query.order_by ? `ORDER BY ${req.query.order_by}` : 'ORDER BY `id` DESC'

    sql.query("SELECT COUNT(*) AS numRows FROM `wallet_histories`" + where, (err, res) => {
        if (err) {
            result(null, err);
        } else {
            numRows = res[0].numRows;
            numPages = Math.ceil(numRows / numPerPage);

            sql.query("SELECT * FROM wallet_histories" + where + " " + orderBy + " LIMIT " + limit, function (err, res2) {
                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                } else {
                    let responsePayload = {
                        results: res2
                    }
                    if (page <= numPages) {
                        responsePayload.pagination = {
                            current: page,
                            perPage: numPerPage,
                            numPages: numPages,
                            firstItem: skip + 1,
                            lastItem: skip + res2.length,
                            totalRecords: numRows,
                            previous: page > 1 ? page - 1 : undefined,
                            next: page < numPages ? page + 1 : undefined
                        }
                    }
                    result(null, responsePayload);
                }
            });
        }
    });
};
WalletHistory.updateById = function (user_id, bat, result) {
    sql.query("UPDATE wallet_histories SET ? WHERE user_id = ? AND type = ?", [bat, user_id, bat.type], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, { status: true, message: 'Record updated successfully.' });
        }
    });
};
WalletHistory.remove = function (ids, result) {
    sql.query("DELETE FROM wallet_histories WHERE id IN (?)", [ids], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

module.exports = WalletHistory;
