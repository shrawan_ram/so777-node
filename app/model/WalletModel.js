'user strict';
var sql = require('./db.js');

//Category object constructor
var Wallet = function (app) {
    this.user = app.user;
    this.user_id = app.user_id;
    this.created_at = new Date();
    this.updated_at = new Date();
};
Wallet.getWalletById = function (slug, result) {
    sql.query("SELECT * FROM `wallets` WHERE `user_id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res)
        }
    });
}
Wallet.getWalletDetail = function (id, result) {
    sql.query("SELECT * FROM `wallets` WHERE `user_id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res)
        }
    });
}
Wallet.getDetails = function (page, result) {
    sql.query("SELECT * FROM `wallets` WHERE `user_id` = ?", [page], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res)
        }
    });
}
Wallet.createWallet = function (newWallet, result) {
    sql.query("INSERT INTO wallets SET ?", newWallet);
    response = {
        status: true,
        message: 'Success! New record added.'
    }
    result(null, response)

};
Wallet.getAllWallet = function (req, result) {
    let numRows,
        queryPagination,
        numPerPage = req.query.limit ?? 25,
        page = parseInt(req.params.page) || 1,
        numPages,
        skip = (page - 1) * numPerPage,
        limit = skip + ',' + numPerPage,
        where = req.query.s ? ` WHERE user LIKE '%${req.query.s}%'` : ' WHERE 1';

    let orderBy = req.query.order_by ? `ORDER BY ${req.query.order_by}` : 'ORDER BY `id` DESC'

    sql.query("SELECT COUNT(*) AS numRows FROM `wallets`" + where, (err, res) => {
        if (err) {
            result(null, err);
        } else {
            numRows = res[0].numRows;
            numPages = Math.ceil(numRows / numPerPage);

            sql.query("SELECT * FROM wallets" + where + " " + orderBy + " LIMIT " + limit, function (err, res2) {
                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                } else {
                    let responsePayload = {
                        results: res2
                    }
                    if (page <= numPages) {
                        responsePayload.pagination = {
                            current: page,
                            perPage: numPerPage,
                            numPages: numPages,
                            firstItem: skip + 1,
                            lastItem: skip + res2.length,
                            totalRecords: numRows,
                            previous: page > 1 ? page - 1 : undefined,
                            next: page < numPages ? page + 1 : undefined
                        }
                    }
                    result(null, responsePayload);
                }
            });
        }
    });
};
Wallet.updateById = function (user_id, bat, result) {
    sql.query("UPDATE wallets SET ? WHERE user_id = ? AND type = ?", [bat, user_id, bat.type], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, { status: true, message: 'Record updated successfully.' });
        }
    });
};
Wallet.remove = function (ids, result) {
    sql.query("DELETE FROM wallets WHERE id IN (?)", [ids], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

module.exports = Wallet;
