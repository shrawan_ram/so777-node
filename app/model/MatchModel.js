'user strict';
var sql = require('./db.js');

//Match object constructor
var Match = function (app) {
    this.name = app.name;
    this.created_at = new Date();
    this.updated_at = new Date();
};
Match.getMatchById = function (event_id, result) {
    sql.query("SELECT * FROM `matches` WHERE `event_id` = ?", [event_id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Match.getMatchDetail = function (id, result) {
    sql.query("SELECT * FROM `matches` WHERE `event_id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Match.getDetails = function (page, result) {
    sql.query("SELECT * FROM `matches` WHERE `event_id` = ?", [page], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Match.createMatch = function (newMatch, result) {
    sql.query("SELECT COUNT(*) AS total FROM matches WHERE event_id = ?", [newMatch.event_id], function (err, res) {
        if (err) {
            result(err, null);
        }
        else {
            let response;
            if (res[0].total == 0) {
                sql.query("INSERT INTO matches SET ?", newMatch);
                response = {
                    status: true,
                    message: 'Success! New record added.'
                }
            } else {
                response = {
                    status: false,
                    errors: [
                        'Error! Match already exists.'
                    ]
                }
            }
            result(null, response)
        }
    });

};
Match.getAllMatch = function (req, result) {
    let numRows,
        queryPagination,
        numPerPage = req.query.limit ?? 25,
        page = parseInt(req.params.page) || 1,
        numPages,
        skip = (page - 1) * numPerPage,
        limit = skip + ',' + numPerPage,
        where1 = req.query.s ? ` matches.name LIKE '%${req.query.s}%'` : ' 1',
        where = req.query.type ? ` matches.type = ${req.query.type}` : ' 1';

    let orderBy = req.query.order_by ? `ORDER BY ${req.query.order_by}` : 'ORDER BY `id` DESC'

    sql.query("SELECT COUNT(*) AS numRows FROM `matches` WHERE" + where, (err, res) => {
        if (err) {
            result(null, err);
        } else {
            numRows = res[0].numRows;
            numPages = Math.ceil(numRows / numPerPage);

            sql.query("SELECT * FROM matches WHERE" + where + " AND " + where1 + " " + orderBy + " LIMIT " + limit, function (err, res2) {
                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                } else {
                    let responsePayload = {
                        results: res2
                    }
                    if (page <= numPages) {
                        responsePayload.pagination = {
                            current: page,
                            perPage: numPerPage,
                            numPages: numPages,
                            firstItem: skip + 1,
                            lastItem: skip + res2.length,
                            totalRecords: numRows,
                            previous: page > 1 ? page - 1 : undefined,
                            next: page < numPages ? page + 1 : undefined
                        }
                    }
                    result(null, responsePayload);
                }
            });
        }
    });
};
Match.updateById = function (id, page, result) {
    sql.query("UPDATE matches SET ? WHERE id = ?", [page, id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, { status: true, message: 'Record updated successfully.' });
        }
    });
};
Match.remove = function (ids, result) {
    sql.query("DELETE FROM matches WHERE id IN (?)", [ids], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

module.exports = Match;
