'user strict';
var sql = require('./db.js');

//Match object constructor
var SettleBat = function (app) {
    this.name = app.name;
    this.created_at = new Date();
    this.updated_at = new Date();
};
SettleBat.getMatchById = function (event_id, result) {
    sql.query("SELECT * FROM `settle_bats` WHERE `event_id` = ?", [event_id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
SettleBat.getMatchDetail = function (id, result) {
    sql.query("SELECT * FROM `settle_bats` WHERE `event_id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
SettleBat.getDetails = function (page, result) {
    sql.query("SELECT * FROM `settle_bats` WHERE `event_id` = ?", [page], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
SettleBat.createMatch = function (newMatch, result) {
    sql.query("SELECT COUNT(*) AS total FROM settle_bats", [newMatch.event_id], function (err, res) {
        if (err) {
            result(err, null);
        }
        else {
            let response;
            sql.query("INSERT INTO settle_bats SET ?", newMatch);
            response = {
                status: true,
                message: 'Success! New record added.'
            }
            result(null, response)
        }
    });

};
SettleBat.getAllMatch = function (req, result) {
    let numRows,
        queryPagination,
        numPerPage = req.query.limit ?? 25,
        page = parseInt(req.params.page) || 1,
        numPages,
        skip = (page - 1) * numPerPage,
        limit = skip + ',' + numPerPage,
        where = req.query.s ? ` settle_bats.name LIKE '%${req.query.s}%'` : ' 1',
        where1 = req.query.type ? ` settle_bats.type = ${req.query.type}` : ' 1',
        where2 = req.query.event_id ? ` settle_bats.event_id = ${req.query.event_id}` : ' 1';

    let orderBy = req.query.order_by ? `ORDER BY ${req.query.order_by}` : 'ORDER BY `id` DESC';

    sql.query("SELECT COUNT(*) AS numRows FROM `settle_bats` WHERE" + where, (err, res) => {
        if (err) {
            result(null, err);
        } else {
            numRows = res[0].numRows;
            numPages = Math.ceil(numRows / numPerPage);

            sql.query("SELECT * FROM settle_bats WHERE" + where + " AND " + where1 + " AND " + where2 + " " + orderBy + " LIMIT " + limit, function (err, res2) {
                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                } else {
                    let responsePayload = {
                        results: res2
                    }
                    if (page <= numPages) {
                        responsePayload.pagination = {
                            current: page,
                            perPage: numPerPage,
                            numPages: numPages,
                            firstItem: skip + 1,
                            lastItem: skip + res2.length,
                            totalRecords: numRows,
                            previous: page > 1 ? page - 1 : undefined,
                            next: page < numPages ? page + 1 : undefined
                        }
                    }
                    result(null, responsePayload);
                }
            });
        }
    });
};
SettleBat.updateById = function (id, page, result) {
    sql.query("UPDATE settle_bats SET ? WHERE id = ?", [page, id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, { status: true, message: 'Record updated successfully.' });
        }
    });
};
SettleBat.remove = function (ids, result) {
    sql.query("DELETE FROM settle_bats WHERE id IN (?)", [ids], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

module.exports = SettleBat;
