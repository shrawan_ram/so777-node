'user strict';
var sql = require('./db.js');

//Category object constructor
var Slider = function (app) {
    this.name = app.name;
    this.created_at = new Date();
    this.updated_at = new Date();
};
Slider.getDetail = function (id, result) {
    sql.query("SELECT * FROM `sliders` WHERE `id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Slider.createSlider = function (newSlider, result) {
    let response;
    sql.query("INSERT INTO sliders SET ?", newSlider);
    response = {
        status: true,
        message: 'Success! New record added.'
    }
    result(null, response)
};
Slider.getAllSlider = function (req, result) {
    let numRows,
        queryPagination,
        numPerPage = req.query.limit ?? 25,
        page = parseInt(req.params.page) || 1,
        numPages,
        skip = (page - 1) * numPerPage,
        limit = skip + ',' + numPerPage,
        where = req.query.s ? ` WHERE name LIKE '%${req.query.s}%'` : ' WHERE 1';

    let orderBy = req.query.order_by ? `ORDER BY ${req.query.order_by}` : 'ORDER BY `id` DESC'

    sql.query("SELECT COUNT(*) AS numRows FROM `sliders`" + where, (err, res) => {
        if (err) {
            result(null, err);
        } else {
            numRows = res[0].numRows;
            numPages = Math.ceil(numRows / numPerPage);

            sql.query("SELECT * FROM sliders" + where + " " + orderBy + " LIMIT " + limit, function (err, res2) {
                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                } else {
                    let responsePayload = {
                        results: res2
                    }
                    if (page <= numPages) {
                        responsePayload.pagination = {
                            current: page,
                            perPage: numPerPage,
                            numPages: numPages,
                            firstItem: skip + 1,
                            lastItem: skip + res2.length,
                            totalRecords: numRows,
                            previous: page > 1 ? page - 1 : undefined,
                            next: page < numPages ? page + 1 : undefined
                        }
                    }
                    result(null, responsePayload);
                }
            });
        }
    });
};
Slider.updateById = function (id, slider, result) {
    sql.query("UPDATE sliders SET ? WHERE id = ?", [slider, id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, { status: true, message: 'Record updated successfully.' });
        }
    });
};
Slider.remove = function (ids, result) {
    console.log([ids]);
    sql.query("DELETE FROM sliders WHERE id IN (?)", [ids], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log(res);
            result(null, res);
        }
    });
};

module.exports = Slider;
