'user strict';
var sql = require('./db.js');

//Gametype object constructor
var Gametype = function (app) {
    this.name = app.name;
    this.slug = app.slug;
    this.created_at = new Date();
    this.updated_at = new Date();
};
Gametype.getGametypeById = function (slug, result) {
    sql.query("SELECT * FROM `game_types` WHERE `slug` = ?", [slug], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Gametype.getGametypeDetail = function (id, result) {
    sql.query("SELECT * FROM `game_types` WHERE `id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Gametype.getDetails = function (page, result) {
    sql.query("SELECT * FROM `game_types` WHERE `slug` = ?", [page], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Gametype.createGametype = function (newGametype, result) {
    sql.query("SELECT COUNT(*) AS total FROM game_types WHERE slug = ?", [newGametype.slug], function (err, res) {
        if (err) {
            result(err, null);
        }
        else {
            let response;
            if (res[0].total == 0) {
                sql.query("INSERT INTO game_types SET ?", newGametype);
                response = {
                    status: true,
                    message: 'Success! New record added.'
                }
            } else {
                response = {
                    status: false,
                    errors: [
                        'Error! Gametype already exists.'
                    ]
                }
            }
            result(null, response)
        }
    });

};

Gametype.getGametype = function (req, result) {

    let where = req.query.category ? ` WHERE category_id = ${req.query.category}` : ' WHERE 1';

    sql.query("SELECT * FROM `game_types`" + where, function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res)
        }
    });
};
Gametype.getAllGametype = async function (req, result) {
    let numRows,
        queryPagination,
        numPerPage = req.query.limit ?? 25,
        page = parseInt(req.params.page) || 1,
        numPages,
        skip = (page - 1) * numPerPage,
        limit = skip + ',' + numPerPage,
        where = req.query.s ? ` name LIKE '%${req.query.s}%'` : ' 1';

    let where1 = req.query.category_id ? `category_id = ${req.query.category_id}` : ' 1 ';

    let orderBy = req.query.order_by ? `ORDER BY ${req.query.order_by}` : 'ORDER BY `id` DESC'

    sql.query("SELECT COUNT(*) AS numRows FROM `game_types` WHERE" + where, async (err, res) => {
        if (err) {
            result(null, err);
        } else {
            numRows = res[0].numRows;
            numPages = Math.ceil(numRows / numPerPage);

            sql.query("SELECT game_types.*, categories.name as category_name FROM `game_types` LEFT JOIN `categories` ON game_types.category_id = categories.id WHERE" + where + " AND " + where1 + " " + orderBy + " LIMIT " + limit, async function (err, res2) {
                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                } else {
                    let responsePayload = {
                        results: res2
                    }
                    if (page <= numPages) {
                        responsePayload.pagination = {
                            current: page,
                            perPage: numPerPage,
                            numPages: numPages,
                            firstItem: skip + 1,
                            lastItem: skip + res2.length,
                            totalRecords: numRows,
                            previous: page > 1 ? page - 1 : undefined,
                            next: page < numPages ? page + 1 : undefined
                        }
                    }
                    result(null, responsePayload);
                }
            });
        }
    });
};
Gametype.updateById = function (id, page, result) {
    sql.query("UPDATE game_types SET ? WHERE id = ?", [page, id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, { status: true, message: 'Record updated successfully.' });
        }
    });
};
Gametype.remove = function (ids, result) {
    sql.query("DELETE FROM game_types WHERE id IN (?)", [ids], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

module.exports = Gametype;
