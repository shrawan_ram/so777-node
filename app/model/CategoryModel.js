'user strict';
var sql = require('./db.js');
var fs = require('fs');

//Category object constructor
var Category = function (app) {
    this.name = app.name;
    this.slug = app.slug;
    this.created_at = new Date();
    this.updated_at = new Date();
};
Category.getCategoryById = function (slug, result) {
    sql.query("SELECT * FROM `categories` WHERE `slug` = ?", [slug], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Category.getCategoryDetail = function (id, result) {
    sql.query("SELECT * FROM `categories` WHERE `id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Category.getDetails = function (page, result) {
    sql.query("SELECT * FROM `categories` WHERE `slug` = ?", [page], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}

Category.createCategory = function (newCategory, result) {

    var img = newCategory.image;
    var data = img.replace(/^data:image\/\w+;base64,/, "");
    function makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    }
    if (newCategory.image != '') {
        var name = makeid(7);
        name = 'IMG_' + name + '.png';
        newCategory.image = name;
        fs.writeFile('upload/category/' + name, data, 'base64', function (err, result) {
            if (err) {
                console.log('error', err);
            }
            else {
                console.log('result', result);
            }
        });
    }

    sql.query("SELECT COUNT(*) AS total FROM categories WHERE slug = ?", [newCategory.slug], function (err, res) {
        if (err) {
            result(err, null);
        }
        else {
            let response;
            if (res[0].total == 0) {
                sql.query("INSERT INTO categories SET ?", newCategory);
                response = {
                    status: true,
                    message: 'Success! New record added.'
                }
            } else {
                response = {
                    status: false,
                    errors: [
                        'Error! Category already exists.'
                    ]
                }
            }
            result(null, response)
        }
    });

};

Category.getCategory = function (req, result) {
    sql.query("SELECT * FROM `categories`", function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res)
        }
    });
};
Category.getAllCategory = async function (req, result) {
    let numRows,
        queryPagination,
        numPerPage = req.query.limit ?? 25,
        page = parseInt(req.params.page) || 1,
        numPages,
        skip = (page - 1) * numPerPage,
        limit = skip + ',' + numPerPage,
        where = req.query.s ? ` c.name LIKE '%${req.query.s}%'` : ' 1',
        // where1 = req.query.category_id ? `c.parent = ${req.query.category_id}` : ' 1 ';

        where1 = req.query.category_id
            ? req.query.category_id != 0 ? `c.parent = ${req.query.category_id}` : `c.parent is null`
            : ' 1 ',
        where2 = req.query.game_type_id ? ` c.game_type_id = ${req.query.game_type_id}` : ' 1';

    let orderBy = req.query.order_by ? `ORDER BY ${req.query.order_by} ` : 'ORDER BY `id` DESC'

    sql.query("SELECT COUNT(*) AS numRows FROM categories c WHERE" + where, async (err, res) => {
        if (err) {
            result(null, err);
        } else {
            numRows = res[0].numRows;
            numPages = Math.ceil(numRows / numPerPage);
            subcategories = [];

            sql.query("SELECT c.*, p.name as parent_name, 0 as subcategories FROM categories c LEFT JOIN categories p ON c.parent = p.id WHERE" + where + " AND " + where1 + " AND " + where2 + " " + orderBy + " LIMIT " + limit, async function (err, res2) {
                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                } else {
                    let subcats;
                    // res2.forEach(async (element, i) => {
                    //     subcats = await getsubcategory(element.id);
                    //     console.log('sub category', subcats);
                    //     // subcategories.push(subcats);
                    //     res2[i].subcategories = subcats;
                    // });

                    for (let i in res2) {
                        subcats = await getsubcategory(res2[i].id);
                        res2[i].subcategories = subcats;
                    }
                    // console.log('sdjkfdskjk', res2);
                    console.log('sdnmfhhds sdfjsd djsfn kidsjf isdjg', res2);
                    let responsePayload = {
                        results: res2
                    }
                    if (page <= numPages) {
                        responsePayload.pagination = {
                            current: page,
                            perPage: numPerPage,
                            numPages: numPages,
                            firstItem: skip + 1,
                            lastItem: skip + res2.length,
                            totalRecords: numRows,
                            previous: page > 1 ? page - 1 : undefined,
                            next: page < numPages ? page + 1 : undefined
                        }
                    }
                    result(null, responsePayload);
                }
            });
        }
    });
};

getsubcategory = async (parent = 0) => {
    let util = require('util');

    const query = util.promisify(sql.query).bind(sql);

    let response = await query("SELECT c.*, p.name as parent_name, 0 as games FROM categories c LEFT JOIN categories p ON c.parent = p.id WHERE c.parent = " + parent)

    let games;
    for (let i in response) {
        games = await getgames(response[i].id);
        response[i].games = games;
    }
    return response ?? [];
}

getgames = async (id = 0) => {
    let util = require('util');

    const query = util.promisify(sql.query).bind(sql);
    let response = await query("SELECT games.*, categories.name as category_name,game_types.name as game_type_name FROM games LEFT JOIN categories ON games.category_id = categories.id LEFT JOIN game_types ON games.game_type_id = game_types.id WHERE games.category_id = " + id)

    return response ?? [];
}
Category.updateById = function (id, category, result) {
    console.log('update category', category);
    var img = category.image;
    var data = img.replace(/^data:image\/\w+;base64,/, "");
    function makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    }
    if (category.image != '') {
        var name = makeid(7);
        name = 'IMG_' + name + '.png';
        category.image = name;
        fs.writeFile('upload/category/' + name, data, 'base64', function (err, result) {
            if (err) {
                console.log('error', err);
            }
            else {
                console.log('result', result);
            }
        });
    }

    sql.query("UPDATE categories SET ? WHERE id = ?", [category, id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, { status: true, message: 'Record updated successfully.' });
        }
    });
};
Category.remove = function (ids, result) {
    sql.query("DELETE FROM categories WHERE id IN (?)", [ids], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

module.exports = Category;
