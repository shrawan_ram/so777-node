'user strict';
var sql = require('./db.js');
const express = require("express");
const bcrypt = require('bcryptjs')

//Category object constructor
var User = function (app) {
    this.name = app.name;
    this.created_at = new Date();
    this.updated_at = new Date();
};
User.getDetail = function (id, result) {
    console.log('dkjfslmf');
    sql.query("SELECT * FROM `admins` WHERE `id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            console.log(res);
            result(null, res[0])
        }
    });
}
User.setPassword = async (password) => {
    const salt = await bcrypt.genSalt(10);
    // now we set user password to hashed password
    let pass = await bcrypt.hash(password, salt);
    return pass;
}
User.createUser = async (newUser, result) => {
    let response;
    let password = await User.setPassword(newUser.password);
    let user = {
        name: newUser.name,
        user_name: newUser.user_name,
        mobile: newUser.mobile,
        password: password,
        email: newUser.email,
        role_id: newUser.role_id,
        admin_id: newUser.admin_id,
    }
    sql.query("SELECT COUNT(*) AS total FROM admins WHERE user_name = ?", [newUser.user_name], function (err, res) {
        if (res[0].total == 0) {
            sql.query("INSERT INTO admins SET ?", user, function (err, res) {
                response = {
                    status: true,
                    message: 'Success! New record added.',
                    data: res.insertId,
                }
                result(null, response)
            });
        } else {
            response = {
                status: false,
                message: 'User name already exits.'
            }
            result(null, response)
        }
    });
};
User.getAllUser = function (req, result) {
    let numRows,
        queryPagination,
        numPerPage = req.query.limit ?? 25,
        page = parseInt(req.params.page) || 1,
        numPages,
        skip = (page - 1) * numPerPage,
        limit = skip + ',' + numPerPage,
        where = req.query.s ? ` WHERE name LIKE '%${req.query.s}%'` : ' WHERE 1';

    if (req.query.admin_id) {
        where += ` AND admin_id = '${req.query.admin_id}'`
    }

    let orderBy = req.query.order_by ? `ORDER BY ${req.query.order_by}` : 'ORDER BY `id` DESC'

    sql.query("SELECT COUNT(*) AS numRows FROM `admins`" + where, (err, res) => {
        if (err) {
            result(null, err);
        } else {
            numRows = res[0].numRows;
            numPages = Math.ceil(numRows / numPerPage);

            sql.query("SELECT admins.*,roles.name as role_name FROM admins LEFT JOIN roles ON admins.role_id = roles.id" + where + " " + orderBy + " LIMIT " + limit, function (err, res2) {
                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                } else {
                    // res2.forEach(element => {
                    //     sql.query("SELECT * FROM admins WHERE user_id = ? ", [element.id], function (err, res5) {
                    //         element.wallet = res5;
                    //         console.log(res5);
                    //     });
                    // });

                    let responsePayload = {
                        results: res2
                    }
                    if (page <= numPages) {
                        responsePayload.pagination = {
                            current: page,
                            perPage: numPerPage,
                            numPages: numPages,
                            firstItem: skip + 1,
                            lastItem: skip + res2.length,
                            totalRecords: numRows,
                            previous: page > 1 ? page - 1 : undefined,
                            next: page < numPages ? page + 1 : undefined
                        }
                    }
                    result(null, responsePayload);
                }
            });
        }
    });
};
User.updateById = async (id, admin, result) => {
    let user = {
        name: admin.name,
        user_name: admin.user_name,
        mobile: admin.mobile,
        email: admin.email,
        role_id: admin.role_id,
        admin_id: admin.admin_id,
    }
    sql.query(`SELECT COUNT(*) AS total FROM admins WHERE user_name = ? AND id NOT IN (?)`, [admin.user_name, id], function (err, res) {
        if (res[0].total == 0) {
            sql.query("UPDATE admins SET ? WHERE id = ?", [user, id], function (err, res) {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                } else {
                    result(null, { status: true, message: 'Record updated successfully.' });
                }
            });
        }
        else {
            response = {
                status: false,
                message: 'User name already exits.'
            }
            result(null, response)
        }
    });
};
User.updatePasswordById = async (id, admin, result) => {
    let password = await User.setPassword(admin.password);

    let user = {
        password: password,
    }
    sql.query("UPDATE admins SET ? WHERE id = ?", [user, id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, { status: true, message: 'Password updated successfully.' });
        }
    });
};
User.remove = function (ids, result) {
    console.log([ids]);
    sql.query("DELETE FROM admins WHERE id IN (?)", [ids], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log(res);
            result(null, res);
        }
    });
};

module.exports = User;
