'user strict';
var sql = require('./db.js');

//Category object constructor
var Bat = function (app) {
    this.user = app.user;
    this.event_name = app.event_name;
    this.created_at = new Date();
    this.updated_at = new Date();
};
Bat.getBatById = function (slug, result) {
    sql.query("SELECT * FROM `bats` WHERE `id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Bat.getBatDetail = function (id, result) {
    sql.query("SELECT * FROM `bats` WHERE `id` = ?", [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Bat.getDetails = function (page, result) {
    sql.query("SELECT * FROM `bats` WHERE `slug` = ?", [page], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res[0])
        }
    });
}
Bat.getUserBats = function (query, id, result) {
    console.log('request', query);
    let where = '';
    if (query.type != '') {
        where = `AND bat_type = '${query.type}'`
    }
    sql.query(`SELECT * FROM bats WHERE user = ? ${where} ORDER BY id DESC`, [id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null)
        } else {
            result(null, res)
        }
    });
}
Bat.createBat = function (newBat, result) {
    sql.query("INSERT INTO bats SET ?", newBat);
    response = {
        status: true,
        message: 'Success! New record added.'
    }
    result(null, response)

};
Bat.getAllBat = function (req, result) {
    let numRows,
        queryPagination,
        numPerPage = req.query.limit ?? 25,
        page = parseInt(req.params.page) || 1,
        numPages,
        skip = (page - 1) * numPerPage,
        limit = skip + ',' + numPerPage,
        where = req.query.s ? `user LIKE '%${req.query.s}%'` : '1',
        where1 = req.query.event_id ? `event_id = ${req.query.event_id}` : '1',
        where3 = req.query.type ? `type = '${req.query.type}'` : '1',
        where2 = req.query.sid ? `sid = ${req.query.sid}` : '1';

    let orderBy = req.query.order_by ? `ORDER BY ${req.query.order_by}` : 'ORDER BY `id` DESC'

    sql.query("SELECT COUNT(*) AS numRows FROM `bats` WHERE " + where, (err, res) => {
        if (err) {
            result(null, err);
        } else {
            numRows = res[0].numRows;
            numPages = Math.ceil(numRows / numPerPage);
            console.log("SELECT * FROM bats WHERE " + where + " AND " + where1 + " AND " + where2 + " AND " + where3 + " " + orderBy + " LIMIT " + limit);

            sql.query("SELECT * FROM bats WHERE " + where + " AND " + where1 + " AND " + where2 + " AND " + where3 + " " + orderBy + " LIMIT " + limit, function (err, res2) {
                if (err) {
                    console.log("error: ", err);
                    result(null, err);
                } else {
                    let responsePayload = {
                        results: res2
                    }
                    if (page <= numPages) {
                        responsePayload.pagination = {
                            current: page,
                            perPage: numPerPage,
                            numPages: numPages,
                            firstItem: skip + 1,
                            lastItem: skip + res2.length,
                            totalRecords: numRows,
                            previous: page > 1 ? page - 1 : undefined,
                            next: page < numPages ? page + 1 : undefined
                        }
                    }
                    result(null, responsePayload);
                }
            });
        }
    });
};
Bat.updateById = function (id, bat, result) {
    sql.query("UPDATE bats SET ? WHERE id = ?", [bat, id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, { status: true, message: 'Record updated successfully.' });
        }
    });
};
Bat.remove = function (ids, result) {
    sql.query("DELETE FROM bats WHERE id IN (?)", [ids], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};

module.exports = Bat;
